<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Master branch</title>
</head>
<body>
    <h1>Titulo de la pagina</h1>
    <div class="local">Esta linea se agrega localmente</div>
    <div class="remoto">modificacion remotamente</div>
    <div>Esta linea se agrega remotamente</div>
    <div class="newClass">
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
        <p>
        Otra modificacion local.
        </p>
    </div>
</body>
</html>